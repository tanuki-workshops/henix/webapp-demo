```yaml
include:
  - '/ci/secure/semgrep.gitlab-ci.yml'
  - '/ci/secrets/gitleaks.gitlab-ci.yml'
  - '/ci/secrets/secure.report.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Security
#-----------------------------------------------------------------------------------------
👮‍♀️:vulnerability:detection:
  stage: 🔎secure
  extends: .semgrep:analyzer  
  needs: ["🎯:testing"]
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

👮‍♀️:secrets:detection:
  stage: 🔎secure
  extends: .gitleaks:analyzer
  needs: ["🎯:testing"]
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

👮‍♀️:secrets:report:
  stage: 🔎secure
  extends: .secure:report
  needs: ["🎯:testing", "👮‍♀️:secrets:detection"]
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
```